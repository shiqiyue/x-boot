package cn.exrick.xboot.dao;

import cn.exrick.xboot.base.XbootBaseDao;
import cn.exrick.xboot.entity.Log;

/**
 * 日志数据处理层
 *
 * @author Exrickx
 */
public interface LogDao extends XbootBaseDao<Log, String> {

}
